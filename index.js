const env = require('./app/environment/environment')
const express = require('express'),
    color = require('colors'),
    body = require('body-parser'),
    cors = require('cors'),
    logger = require('morgan');
const passport = require('passport')
mariadb = require('mariadb') //global
mongoose = require('mongoose') //global
app = express() //global

module.exports.pool = pool = mariadb.createPool({ ...env.mariadb})


const port = env.port || process.env.port || 3000
const model = require('./app/models')
const routes = require('./app/routing')
console.clear()
console.log(`path : ${__dirname}`.bgRed)

env.isProd ? null : app.use(logger('dev'))
app.use(body.urlencoded({ extended: true }))
app.use(body.json())

app.map = (a, route) => {
    route = route || ''
    for (var key in a) {
        switch (typeof a[key]) {
            case 'object':
                app.map(a[key], route + key);
                break
            case 'function':
                app[key](route, a[key])
                break;
        }
    }
}
// mariadb conection
async function asyncFunction() {
    let conn
    try {
        conn = await pool.getConnection()
        // console.table(conn)
        // console.table(await conn.query('show tables'))
        console.log('connectedPool',await pool.totalConnections())
        // console.table(await conn.query('desc users'))
        // const res = await conn.query("INSERT INTO myTable value (?, ?)", [1, "mariadb"]);
        // console.log(res); // { affectedRows: 1, insertId: 1, warningStatus: 0 }
    } catch (error) {
        console.log('error', error)
    } finally {
        if (conn) return conn.end()
    }
}

asyncFunction()
mongoose.connect(env.server, { useNewUrlParser: true, useUnifiedTopology: true })
mongoose.connection.on('error', console.error.bind(console, 'connections error:'))
mongoose.connection.once('open', () => console.log(`Connected at ${env.server}`.bgBlue.bold))

require('./app/middleware/passport').init()

// console.log('module', module)
// console.log('process', process)

// require('./app/middleware').initPDF() //init pdf sample
app.map({ ...routes })
app.use(cors())
app.use(express.static('./app/assets'))
app.listen(port, () => console.log(color.bold('Listening at: ').bgRed, port))