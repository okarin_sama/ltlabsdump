module.exports = {
    port: 3000,
    isProd: false,
    secret: 'is secret',
    server: 'mongodb://localhost/ltlabs',
    mariadb: {
        host: 'localhost', 
        user: 'root',
        password: '',
        port: 3307,
        connectionLimit: 5,
        database: 'ltlab',
        // rowsAsArray: false,
        // queryTimeout: 5000,
        // compress: false,s
        // queueLimit: 30,
        // acquireTimeout: 1000000
    },
    elastic:{
        domain: "search-ltlabs-es-2kdqv2pomz333jwg2ktze4l6za.us-east-2.es.amazonaws.com"
    },
    keys:{
        privName: 'rsa_private.pem',
        pubName: 'rsa_public.pem',
    }
}