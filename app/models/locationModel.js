const {Schema} = mongoose
module.exports = mongoose.model('Location', new Schema({
    latitude: Number,
    longitude: Number
}))