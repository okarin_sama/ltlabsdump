
const { Schema } = mongoose;
module.exports = mongoose.model('User', new Schema({
    username: {type: String, required: [true, 'input a username and should be unique']},
    password: {type: String, required: [true, 'input a password and should be unique']},
    salt: String
})
)