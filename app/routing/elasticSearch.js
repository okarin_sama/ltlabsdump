const {elastic} = require('../environment/environment')
const elasticFn = require('../middleware/elastic')
const handler = require('../middleware/handler').elastic
const https = require('https')
const elasticSE= {
    searchPost: (req,res) => {
        let {scroll} = req.body
        // elasticFn.query(query,fields)
        let request = elasticFn.setState(req.body)
        // console.log(JSON.stringify(request))
        handler.request(request,'accounts','_search')
        .then(ev => res.send(ev))
        .catch(err => res.status(500).send({message: err}))
    },
    searchGet: (req,res) => {
        
    },
    insertBulk: (req, res) => {
    },
    getRoot: (req,res) => {
        https.post(elastic.domain, expect => {
            expect.on('data', data => {
                res.send(JSON.parse(data))
            })
        })
    }
}
const elasticMod ={
    '/es': {
        get: elasticSE.getRoot,
        '/search':{
            post: elasticSE.searchPost,
            get: elasticSE.searchGet
        },
        '/count': {},
        '/insert':{
            post: elasticSE.insertBulk
        },
        '/update':{},
        '/delete':{}
    }

}

module.exports = {...elasticMod}