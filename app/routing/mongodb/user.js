const user = mongoose.model('User')
const middleware = require('../../middleware/handler')
const passport = require('../../middleware/passport')
const handler= middleware.responseHandler
const {hashPassword} = middleware

var users = {
    list: (req, res) => {
        user.find({}, (err, callback) => handler(err, callback, res))
    },
    get: (req, res) => {
        let { uid } = req.params
        user.findById(uid, (err, callback) => handler(err, callback, res))
    },
    delete: (req, res) => {
        let { uid } = req.params
        user.findByIdAndDelete(uid, (err, callback) => handler(err, callback, res))
    },
    create: (req, res) => {
        let { username, password } = req.body
        user.countDocuments((err, count) =>{
            if(err) res.status(402).send(err)
            else {
                count != 0 ? res.status(402).send({username: 'Already exist'}) :
                hashPassword({password}).then(ev => {
                    console.log('event', ev)
                    new user({ username, password: ev['hash'], ...ev }).save((err, callback) => handler(err, callback, res))
                })
            }
        })
    }
}

var userModule = {
    '/users': {
        get: (passport.auth,users.list),
        '/:uid': {
            get: users.get,
            delete: users.delete,
        }
    },
    '/create/user': {
        post: users.create
    }
}

module.exports = userModule