const location = require('./locations'),
login = require('./login'),
user = require('./user');

module.exports = {...location, ...login, ...user}