const locations = mongoose.model('Location')
const handler = require('../../middleware/handler').responseHandler
var location = {
    list: (req, res) => {
        locations.find({}, (err, callback) => handler(err, callback, res))
    },
    get: (req, res) => {
        let {lid} = req.params
        locations.findById(lid, (err, callback) => handler(err, callback, res))
    },
    delete: (req, res) => {
        let {dlid} = req.params
        locations.findByIdAndDelete(dlid, (err, callback) => handler(err, callback, res))
    },
    create: (req, res) => {
        let {latitude, longitude} = req.body
        new locations({latitude, longitude}).save((err, callback) => handler(err, callback, res))

    }
}
module.exports = locationModule ={
    '/location':{
        get: location.list,
        '/:lid': {get: location.get},
        '/:dlid':{delete:location.delete},
    },
    '/create/location':{
        post: location.create
    }
}