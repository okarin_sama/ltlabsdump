const middleware = require('../../middleware')
const passport = require('../../middleware/passport')
const {handleQuery} = middleware.mariaDBHandle
const { hashPassword } = middleware
const root = '/mariadb'
const userid = 'user_id'
const tableName = 'users'
var users = {
    list: (req, res) => {
        handleQuery(`select * from ${tableName}`, true , res)
    },
    get: (req, res) => {
        let { uid } = req.params
        handleQuery(`select * from ${tableName} where ${userid}='${uid}'`,true,res)
    },
    delete: (req, res) => {
        let { uid } = req.params
        handleQuery(`delete from ${tableName} where ${userid} = '${uid}'`,true, res)
    },
    create: (req, res) => {
        let { username, password } = req.body
        handleQuery(` select count(*) as users from ${tableName} where username='${username}'`, false,res,false).then(ev => {
            console.log('return', ev[0].users)
            ev[0].users != 0 ?  res.status(402).send({username: 'Already exist'}) : 
            hashPassword({password}).then(ev => {
                console.log('event', ev)
                handleQuery(`insert into ${tableName} values(null, '${username}' , '${ev["hash"]}', '${ev["salt"]}')`, false,null).then(ev => {
                    middleware.tokenize({username}, '1h').then(ev => res.send({token: ev, username}))
                })
            })
        })

    },
    update: (req, res) =>{
        let { username, password } = req.body
        let {uid} = req.params
        handleQuery(`select count(*) as users from ${tableName} where username = '${username}' && user_id != ${uid}`, false,res,false).then(ev => {
            ev[0].users != 0 ? res.status(402).send({username: 'You cant change another existing username'}) :
            hashPassword({password}).then(ev => {
                handleQuery(`update ${tableName} set username='${username}', password='${ev.hash}', salt='${ev.salt}' where ${userid} =${uid}`,true,res)
            })
        })
        

    }
}

var userModule = {
    [`${root}/users`]: {
        get: users.list,
        '/:uid': {
            get: (passport.auth(), users.get),
            delete: users.delete,
            put: users.update
        }
    },
    [`${root}/create/user`]: {
        post: users.create
    }
}

module.exports = userModule