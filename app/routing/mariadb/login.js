const {secret} = require('../../environment/environment')
const middleware = require('../../middleware/handler')
const {authenticate} = middleware.mariaDBHandle
const root = 'mariadb'
var login ={
    authenticate: (req, res) => {
        authenticate(req.body, res, 'users').then(ev => {
            // let processSession = ({isLoggedIn}) => {
            //     if(isLoggedIn){
            //         req.session.regenerate(() => {
            //             req.session.user = ev
            //             req.session.success = 'Authenticated as ' + ev.username
            //         })
            //     }
            //     else{
            //         req.session.error = 'Auth failed'
            //     }
            //     return new Promise(expect => expect())
            // }
            // processSession(ev).then(() => res.send({...ev, ...req.session}) )
            middleware.tokenize({username: ev.username},'1h').then(token => res.send({token: token, ...ev }))
        })
    }
}

module.exports = {
    [`/${root}/login`] : {
        post: login.authenticate
    }
}