const mongod = require('./mongodb'),
mariadb = require('./mariadb'),
elastic = require('./elasticSearch')
module.exports = {...mongod, ...mariadb,...elastic}