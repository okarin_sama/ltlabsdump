const handler = require('./handler')
const passport = require('./passport')
const puppeteer = require('./puppeteer')
const elastic = require('./elastic')
module.exports = {...passport, ...handler , ...puppeteer, ...elastic}