const {Strategy,ExtractJwt} = require('passport-jwt')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const fs = require('fs')
const env = require('../environment/environment')
const pub_key = fs.readFileSync(__dirname+'/../jwtKeys/'+env.keys.pubName, 'utf8')
const priv_key = fs.readFileSync(__dirname+'/../jwtKeys/'+env.keys.privName, 'utf8')
const user = mongoose.model('User')
const middleware = require('./index')

const userid = 'user_id'
const tableName = 'users'

const options = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: pub_key,
    algorithms: ['RS256']
}
module.exports = {
    auth : () => {
        app.get('*',passport.authenticate('jwt',{session: false}), (req, res, next) => {
            next()
        })
    },
    init: () => {
        passport.use(new Strategy(options, (payload, done) => {
            // console.log(payload)

            // ====mongodb
            // user.findOne({...payload.username}, (err, callback) =>{
            //     return done(err, payload)
            // })

            // ====mariadb
            middleware.mariaDBHandle.handleQuery(`select count(*) as users from ${tableName} where username='${payload.username}'`,false,null, false).then(ev => {
                if(ev[0].users != 0 ) return done(null,payload)
                else return done(null,false)
            })
        }))
    }
}