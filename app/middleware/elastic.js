const es = {
    /**
     * Sets the query state given by the req.body
     * @param body req.body data 
     */
    setState: (body) => {
        return {
            ...es.paginate(body),
            ...es.setSize(body),
            ...es.sort(body),
            ...es.query(body),
            ...es.highlight(body),
        }
    },
    /**
     * Sets the scroll next value
     * @param scroll API timeout of the token aws
     * @param scroll_id API token key from the aws elastic
     */
    setScroll: ({scroll, scroll_id}) => {
        return {
            scroll,scroll_id
        }
    },
    /**
     * Set the callback's size to return query
     * @param size Numeric | String 
     */
    setSize: ({ size }) => {
        return {size}
    },
    /**
     * Sorts the callback query
     * @param field String : Column or Field type 
     * @param order asc | desc | string : The sort order
     */
    sort: ({ fields, order }) => {
        // "sort": [
        //     { "account_number": "asc" }
        //   ],
       if(fields != undefined || fields != null){
        if(Array.isArray(fields)){
            return {
                sort: {
                    [es.sanitizeFields(fields[0])]: { order: order }
                }
            }
        }
        else{
            return {
                sort:{
                    [es.sanitizeFields(fields)]:{order: order}
                }
            }
        }
       }
    },
    /**
     * Sets query object
     * @param query Text value to search
     * @param fields Array[String] | String : Query value fields
     */
    query: ({ query, fields }) => {
        // "match": { "address": "mill lane" }
        // "match_phrase": { "address": "mill lane" }
        // "bool": {
        //     "must": [
        //       { "match": { "age": "40" } }
        //     ],
        //     "must_not": [
        //       { "match": { "state": "ID" } }
        //     ]
        //   }
        if(query == undefined){
            return {
                query:{
                    match_all: {}
                }
            }
        }
        else{
            if (Array.isArray(fields)) {
                return {
                    query: {
                        multi_match: {
                            query: query,
                            fields: [...fields]
                        }
                    }
                }
            }
            else if (fields == undefined) {
                return {
                    query: {
                        query_string: {
                            query: query
                        }
                    }
                }
            }
            else return {
                query: {
                    query_string: {
                        default_field: fields,
                        query: query
                    }
                }
            }
        }
    },
    sanitizeFields: (field) => {
        let index = field == undefined || field == null? -1 :  field.indexOf('^')
        index == -1 ? null : field = field.substring(index, 0)
        return index == -1 ? field : field = field.substring(index, 0)
    },
    /**
     * Highlights the retrieved value <em>{search_value}</em>
     * @param fields String : Column or Field type
     */
    highlight: ({ fields }) => {
        // "pre_tags" : ["<tag1>"],
        // "post_tags" : ["</tag1>"],
        if(fields != undefined || fields != null){
            if (Array.isArray(fields)) {
                let fieldsArr = fields.reduce((prev, cur) => {
                    return prev = { ...prev, [es.sanitizeFields(cur)]: {} }
                }, {})
                return {
                    highlight: {
                        fields: {
                            ...fieldsArr
                        }
                    }
                }
            }
            else {
                return {
                    highlight: {
                        fields: {
                            [fields]: {}
                        }
                    }
                }
            }
        }
    },
    /**
     * Return a specified offset of the callback query
     * @param from Numeric|String: Index of the query from the top
     * @param size The buffer load from the index of "from" field
     */
    paginate: ({ from, size }) => {
        return {
            from,
            size
        }
    }
}
module.exports = {
    ...es
}