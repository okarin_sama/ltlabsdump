const puppeteer = require('puppeteer')


module.exports = {
    initPDF: async () => {
        const browser = await puppeteer.launch()
        const page = await browser.newPage()
        await page.goto('https://www.google.com.ph', {waitUntil: 'networkidle2'})
        // await page.pdf({path: './app/pdf/sample.pdf', format: 'A4'})

        const demoCollector = await page.evaluate((e) =>{
            return {
                document: document.querySelector('html').getAttribute('itemtype'),
                localStorage: localStorage.length
            }
        })
        console.log('collect', demoCollector)
        await browser.close()
    }
}