const hasher = require("pbkdf2-password")()
const user = mongoose.model('User')
const env = require('../environment/environment')
const mariadb = require('mariadb') //global
const jwt = require('jsonwebtoken')
const fs = require('fs')
const pub_key = fs.readFileSync(__dirname + '/../jwtKeys/' + env.keys.pubName, 'utf8')
const priv_key = fs.readFileSync(__dirname + '/../jwtKeys/' + env.keys.privName, 'utf8')
var { pool } = require('../../index')
const https = require('https')

module.exports = {
    responseHandler: (err, response, res) => {
        if (err) res.status(402).send(err)
        else res.status(200).send(response)
    },
    authenticate: ({ username, password }, res) => {
        return new Promise(expect => {
            user.findOne({ username }, (err, callback) => {
                if (err) expect({ isLoggedIn: false, message: err })
                else {
                    callback == null ? expect({ isLoggedIn: false, message: 'Not Existing User..' }) :
                        hasher({ password, salt: callback['salt'] }, (err, pass, salt, hash) => {
                            if (err) expect({ isLoggedIn: false, message: err })
                            else expect(hash == callback['password'] ? { isLoggedIn: true, hash, salt, pass, username } : { isLoggedIn: false, message: 'Incorrect Password' })
                        })
                }
            })
        })
    },
    hashPassword: ({ password }) => {
        return new Promise(res => {
            hasher({ password }, (err, pass, salt, hash) => {
                if (err) res.status(402).send(err)
                else res({ pass, salt, hash })
            })
        })
    },
    tokenize: (payload, expiration) => {
        return new Promise(expect => {
            jwt.sign(payload, priv_key, { algorithm: 'RS256', expiresIn: expiration }, (err, token) => {
                if (err) expect({ error: err })
                else expect(token)
            })
        })
    },
    unTokenize: (token) => {
        return new Promise(expect => {
            jwt.verify(token, pub_key, { algorithms: ['RS256'] }, (err, payload) => {
                if (err) expect({ error: err })
                else expect(payload)
            })
        })
    },
    mariaDBHandle: {
        handleQuery: (query, isGet, res, asArray) => {
            if (isGet) {
                async function list() {
                    let conn
                    try {
                        conn = await pool.getConnection()
                        // conn.query('select * from users').then(ev => res.send(ev)) //this is a promise base query
                        res.send(await conn.query(query))
                    } catch (error) {
                        res.status(402).send({ message: error.message })
                    } finally { if (conn) return conn.end() }
                }
                list()
            }
            else {
                return new Promise(expect => {
                    async function returnQuery() {
                        let conn
                        try {
                            if (asArray) pool = mariadb.createPool({ ...env.mariadb, rowsAsArray: true })
                            conn = await pool.getConnection()
                            expect(await conn.query(query))
                        } catch (error) {
                            res.status(402).send({ message: error.message })
                        } finally { if (conn) return conn.end() }
                    }
                    returnQuery()
                })
            }
        },
        authenticate: ({ username, password }, res, table) => {
            return new Promise(expect => {
                async function auth() {
                    let conn
                    try {
                        conn = await pool.getConnection()
                        // res.send(await conn.query(query))
                        console.log(table)
                        let res = await conn.query(`select * from ${table} where username='${username}'`)
                        let user = res[0]
                        user == undefined ? expect({ isLoggedIn: false, message: 'Not Existing User..' }) :
                            hasher({ password, salt: user.salt }, (err, pass, salt, hash) => {
                                if (err) res.status(402).send({ isLoggedIn: false, message: err })
                                else expect(hash == user['password'] ? { isLoggedIn: true, hash, salt, pass, username } : { isLoggedIn: false, message: 'Incorrect Password' })
                            })

                    } catch (error) {
                        res.status(402).send({ isLoggedIn: false, message: error })
                    } finally { if (conn) return conn.end() }
                }
                auth()
            })
        },
    },
    elastic: {
        post: (body, index,type) => {
            console.log('body', body , JSON.stringify(body))
            return new Promise((expect, error) => {
                let options = {
                    hostname: env.elastic.domain, 
                    headers: {
                        "Content-Type": "application/json"
                    },
                    path: `/${index}/${type}`,
                    method: 'POST',
                }
                try {
                    let req = https.request(options, res => {
                        res.on('data', data => {
                            expect(JSON.parse(data))
                        })
                    })
                    req.on('error', e => {
                        error(e.message)
                    })
                    req.write(JSON.stringify(body))
                    req.end()   
                } catch (error) {
                    console.error(error)
                }
            })
        },
        request:(body, index,type) => {
            console.log('body', body)
            return new Promise((expect, error) => {
                let options = {
                    url: env.elastic.domain+`/${index}/${type}`, 
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: 'POST',
                    body: JSON.stringify(body)
                }
                request(options,(err, callback) => {
                    console.log('callback', callback)
                    if(err) error(err)
                    else expect(callback)
                })
                
            })
        },
    }
}